import Plyr from 'plyr';

// ----------------------------------------------------------------------------
// DATA
// ----------------------------------------------------------------------------

const root = document.documentElement;

// BOOKMARKING SYSTEM //

// Get bookmarks HTML elements
const bookmarkEl = document.querySelector('.audiobook__bookmark');
const bookmarkEls = document.querySelectorAll('.audiobook-preview__bookmark');

// Declare AJAX URL for development and production environments
const ajaxUrl = {
    dev: new URL('https://libreaudio.test/ajax'),
    prod: new URL('https://libreaudio.fr/ajax')
};

// AUDIO PLAYER //

// Create new instance of Plyr
const plyr = new Plyr('#plyr', {
    controls: [
        'play',
        'progress',
        'current-time',
        'duration',
        'mute',
        'volume'
    ],
    tooltips: {
        controls: false,
        seek: false
    },
    i18n: {
        'play': 'Lire',
        'pause': 'Mettre sur pause',
        'seek': 'Parcourir la piste audio',
        'currentTime': 'Temps écoulé depuis le début de la piste audio',
        'duration': 'Durée de la piste audio',
        'volume': 'Ajuster le volume',
        'mute': 'Couper le son',
        'unmute': 'Réactiver le son'
    }
});

// Get audio player header HTML elements
const audioPlayerHeaderEl = document.querySelector('.audio-player__header');
const audioPlayerPlyrEl = document.querySelector('.plyr');
const audioPlayerDataEl = document.querySelector('.audio-player__data');
const audioPlayerAudiobookTitleEl = document.querySelector('.audio-player__audiobook-title');
const audioPlayerTrackTitleEl = document.querySelector('.audio-player__active-track-title');
const audioPlayerPreviousBtn = document.querySelector('.audio-player__previous-button');
const audioPlayerNextBtn = document.querySelector('.audio-player__next-button');

// Get playlist HTML elements and declare activeId variable
const audioPlayerPlaylistEls = document.querySelectorAll('.audio-player__playlist li');
let audioPlayerActiveId = null;

// MOBILE NAVIGATION //

// Get navigation button, menu, menu list and overlay elements
const navBtn = document.querySelector('.top-nav__burger-button');
const navMenuEl = document.querySelector('.top-nav__menu');
const navMenuListEls = document.querySelectorAll('.top-nav__menu li');
const overlayEl = document.querySelector('.overlay');

// Declare CSS media queries related object
const mediaQuery = {
    tabletWidth: 768,
    desktopWidth: 992
}

// Declare touch related object
const touchPosition = {
    initialX: null,
    initialY: null
};

// ANIMATIONS //

// Get header, search bar HTML elements, back to top button and featured audiobooks
const headerEl = document.querySelector('header');
const searchBarEl = document.querySelector('.library__search-bar');
const backToTopBtn = document.querySelector('.back-to-top-button');
const mostRecentAudiobook = document.querySelector('.featured__most-recent-audiobook');
const mostViewedAudiobook = document.querySelector('.featured__most-viewed-audiobook');

// Declare scroll related object
const scrollOffset = {
    start: 0,
    header: 0,
    searchBar: 0
};

// ----------------------------------------------------------------------------
// FUNCTIONS
// ----------------------------------------------------------------------------

// BOOKMARKING SYSTEM //

// Toggle current audiobook bookmark style
function toggleBookmarkStyle(e) {
    if (e.currentTarget.classList.contains('audiobook__bookmark')) {
        e.currentTarget.classList.toggle('audiobook__bookmark--filled');
    } else if (e.currentTarget.classList.contains('audiobook-preview__bookmark')) {
        e.currentTarget.classList.toggle('audiobook-preview__bookmark--filled');
    }
}

// Edit current audiobook bookmark title tag
function editBookmarkTitle(audiobook, audiobookTitle) {
    if (audiobook.classList.contains('audiobook__bookmark--filled') || audiobook.classList.contains('audiobook-preview__bookmark--filled')) {
        audiobook.setAttribute('aria-label', 'Retirer '+ audiobookTitle + ' de vos livres audio favoris');
    } else {
        audiobook.setAttribute('aria-label', 'Ajouter ' + audiobookTitle + ' à vos livres audio favoris');
    }
}

// Send current audiobook ID and fetch title string to edit bookmark title tag
function sendIdFetchTitle(e) {
    const audiobook = e.currentTarget;
    const audiobookId = audiobook.getAttribute('audiobook-id');
    ajaxUrl.prod.searchParams.set('audiobookId', audiobookId);
    fetch(ajaxUrl.prod)
        .then(response => response.json())
        .then(function(data) {
            editBookmarkTitle(audiobook, data);
        })
        .catch(err => console.error(err));
}

// Toggle current audiobook bookmark style and edit bookmark title tag
function toggleBookmark(e) {
    toggleBookmarkStyle(e);
    sendIdFetchTitle(e);
}

// Handle bookmarks on click event
function toggleBookmarksOnClick() {
    if (bookmarkEl) {
        bookmarkEl.addEventListener('click', toggleBookmark);
        bookmarkEl.addEventListener('keyup', function(e) {
            if (e.keyCode === 13) { // Enter key
                toggleBookmark(e);
            }
        });
    } else if (bookmarkEls.length > 0) {
        for (let i = 0; i < bookmarkEls.length; i++) {
            bookmarkEls[i].addEventListener('click', toggleBookmark);
            bookmarkEls[i].addEventListener('keyup', function(e) {
                if (e.keyCode === 13) { // Enter key
                    toggleBookmark(e);
                }
            });
        }
    }
}

// AUDIO PLAYER //

// Get selected track id
function getSelectedId(el) {
    return Number(el.getAttribute('data-id'));
}

// Get selected track source
function getSelectedSource(el) {
    return el.getAttribute('data-src');
}

// Get selected track type
function getSelectedType(el) {
    return el.getAttribute('data-type');
}

// Set active track source, type and playing state based on id
function setActiveSource(id, source, type, isPlaying) {
    if (audioPlayerActiveId !== id) {
        audioPlayerActiveId = id;
        plyr.source = {
            type: 'audio',
            sources: [{
                src: source,
                type: type
            }]
        };
        for (let i = 0; i < audioPlayerPlaylistEls.length; i++) {
            if (Number(audioPlayerPlaylistEls[i].getAttribute('data-id')) === id) {
                audioPlayerPlaylistEls[i].className = 'audio-player__active-track';
            } else {
                audioPlayerPlaylistEls[i].className = '';
            }
        }
        if (isPlaying) {
            plyr.play();
        }
    } else {
        plyr.togglePlay();
    }
}

// Enable audio player data element single (and first) automatic scroll animation
function enableDataSingleAutoScroll() {
    if (audioPlayerDataEl) {
        let audioPlayerDataContent = audioPlayerDataEl.querySelector('cite');
        let autoScrollDuration = audioPlayerDataContent.getBoundingClientRect().width / 60;
        if (root) {
            root.style.setProperty('--auto-scroll-duration', autoScrollDuration + 's');
        }
        audioPlayerDataEl.classList.remove('audio-player__data--infinite-auto-scroll');
        audioPlayerDataEl.classList.add('audio-player__data--single-auto-scroll');
    }
}

// Enable audio player data element infinite automatic scroll animation
function enableDataInfiniteAutoScroll() {
    if (audioPlayerDataEl) {
        let audioPlayerDataContent = audioPlayerDataEl.querySelector('cite');
        let autoScrollDuration = (audioPlayerDataEl.getBoundingClientRect().width + audioPlayerDataContent.getBoundingClientRect().width) / 60;
        if (root) {
            root.style.setProperty('--audio-player-data-width', audioPlayerDataEl.getBoundingClientRect().width + 'px');
            root.style.setProperty('--auto-scroll-duration', autoScrollDuration + 's');
        }
        audioPlayerDataEl.classList.remove('audio-player__data--single-auto-scroll');
        audioPlayerDataEl.classList.add('audio-player__data--infinite-auto-scroll');
    }
}

// Disable audio player data element automatic scroll animation
function disableDataAutoScroll() {
    if (audioPlayerDataEl) {
        audioPlayerDataEl.classList.remove('audio-player__data--single-auto-scroll');
        audioPlayerDataEl.classList.remove('audio-player__data--infinite-auto-scroll');
        audioPlayerDataEl.removeEventListener('animationend', enableDataInfiniteAutoScroll);
    }
}

// Toggle audio player data element automatic scroll animation depending on data element and content width
function toggleDataAutoScroll() {
    if (audioPlayerDataEl) {
        let audioPlayerDataContent = audioPlayerDataEl.querySelector('cite');
        if (audioPlayerDataEl.getBoundingClientRect().width < audioPlayerDataContent.getBoundingClientRect().width) {
            enableDataSingleAutoScroll();
            audioPlayerDataEl.addEventListener('animationend', enableDataInfiniteAutoScroll);
        } else {
            disableDataAutoScroll();
        }
    }
}

// Toggle audio player data element automatic scroll animation on intersetion
function toggleDataAutoScrollOnIntersect() {
    if (audioPlayerDataEl) {
        const dataIntersectionObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    toggleDataAutoScroll();
                    observer.unobserve(entry.target);
                }
            });
        });
        dataIntersectionObserver.observe(audioPlayerDataEl);
    }
}

// Toggle audio player data element automatic scroll animation on resize
function toggleDataAutoScrollOnResize() {
    if (audioPlayerDataEl) {
        const dataResizeObserver = new ResizeObserver(function(entries) {
            entries.forEach(function(entry) {
                toggleDataAutoScroll();
            })
        });
        dataResizeObserver.observe(audioPlayerDataEl);
    }
}

// Set active track title into audio player
function setTrackTitle() {
    const activeTrackEl = document.querySelector('.audio-player__active-track');
    const activeTrackTitleEl = activeTrackEl.querySelector('.audio-player__track-title');
    if (audioPlayerTrackTitleEl.innerHTML !== activeTrackTitleEl.innerHTML) {
        audioPlayerTrackTitleEl.innerHTML = activeTrackTitleEl.innerHTML;
        disableDataAutoScroll();
        toggleDataAutoScroll();
    }
}

// Set first track as active track
function selectFirstTrack() {
    setActiveSource(getSelectedId(audioPlayerPlaylistEls[0]), getSelectedSource(audioPlayerPlaylistEls[0]), getSelectedType(audioPlayerPlaylistEls[0]), false);
    setTrackTitle();
    disableDataAutoScroll();
    for (let i = 0; i < audioPlayerPlaylistEls.length; i++) {
        disableTrackPlayingStyle(audioPlayerPlaylistEls[i]);
    }
}

// Set previous track as active track
function selectPreviousTrack(e) {
    const previous = audioPlayerActiveId - 1;
    if (previous >= 0) {
        setActiveSource(getSelectedId(audioPlayerPlaylistEls[previous]), getSelectedSource(audioPlayerPlaylistEls[previous]), getSelectedType(audioPlayerPlaylistEls[previous]), true);
        setTrackTitle();
    }
}

// Set next track as active track
function selectNextTrack(e) {
    const next = audioPlayerActiveId + 1;
    if (next < audioPlayerPlaylistEls.length) {
        setActiveSource(getSelectedId(audioPlayerPlaylistEls[next]), getSelectedSource(audioPlayerPlaylistEls[next]), getSelectedType(audioPlayerPlaylistEls[next]), true);
        setTrackTitle();
    }
}

// Set targeted track as active track
function selectTargetedTrack(e) {
    setActiveSource(getSelectedId(e.currentTarget), getSelectedSource(e.currentTarget), getSelectedType(e.currentTarget), true);
    setTrackTitle();
}

// Set active track on playlist click event
function setActiveTrackOnPlaylistClick() {
    for (let i = 0; i < audioPlayerPlaylistEls.length; i++) {
        audioPlayerPlaylistEls[i].addEventListener('click', selectTargetedTrack);
        audioPlayerPlaylistEls[i].addEventListener('keyup', function(e) {
            if (e.keyCode === 13) { // Enter key
                selectTargetedTrack(e);
            }
        });
    }
}

// Set active track on previous button click event
function setActiveTrackOnPreviousBtnClick() {
    if (audioPlayerPreviousBtn) {
        audioPlayerPreviousBtn.addEventListener('click', selectPreviousTrack);
        audioPlayerPreviousBtn.addEventListener('keyup', function(e) {
            if (e.keyCode === 13) { // Enter key
                selectPreviousTrack(e);
            }
        });
    }
}

// Set active track on next button click event
function setActiveTrackOnNextBtnClick() {
    if (audioPlayerNextBtn) {
        audioPlayerNextBtn.addEventListener('click', selectNextTrack);
        audioPlayerNextBtn.addEventListener('keyup', function(e) {
            if (e.keyCode === 13) { // Enter key
                selectNextTrack(e);
            }
        });
    }
}

// Set active track on Plyr ended event
function setActiveTrackOnTrackEnd() {
    plyr.on('ended', selectNextTrack);
}

// Edit Plyr button aria-label attribute
function editPlyrBtnAriaLabel(el) {
    el.setAttribute('aria-label', el.getAttribute('aria-label') + ' ' + audioPlayerAudiobookTitleEl.innerHTML + ' – ' + audioPlayerTrackTitleEl.innerHTML);
}

// Edit Plyr play button aria-label attribute on multiple media events
function editPlyrBtnAriaLabelOnMediaEvents() {
    ['loadstart', 'playing', 'timeupdate', 'pause'].forEach(function(e) {
        plyr.on(e, function(e) {
            editPlyrBtnAriaLabel(document.querySelector('[data-plyr="play"]'));
        });
    });
}

// Edit Plyr controls item time format from 00:00 to 0:00:00
function editPlyrControlsItemTimeFormat(el) {
    if (el.innerHTML.length === 5) {
        el.innerHTML = '0:' + el.innerHTML;
    }
}

// Edit Plyr controls current time item on multiple media events
function editPlyrCurrentTimeOnMediaEvents() {
    ['loadstart', 'timeupdate', 'seeking', 'seeked'].forEach(function(e) {
        plyr.on(e, function(e) {
            editPlyrControlsItemTimeFormat(document.querySelector('.plyr__time--current'));
        });
    });
}

// Edit Plyr controls duration item on loaded data
function editPlyrDurationOnLoadedData() {
    plyr.on('loadeddata', function(e) {
        editPlyrControlsItemTimeFormat(document.querySelector('.plyr__time--duration'));
    });
}

// Get track play arrow and waveform elements and enable playing style
function enableTrackPlayingStyle(el) {
    const playArrowEl = el.querySelector('.audio-player__track-play-arrow');
    playArrowEl.classList.remove('audio-player__track-play-arrow--visible');
    const waveformEl = el.querySelector('.audio-player__track-waveform');
    waveformEl.classList.add('audio-player__track-waveform--visible');
}

// Get track play arrow and waveform elements and disable playing style
function disableTrackPlayingStyle(el) {
    const waveformEl = el.querySelector('.audio-player__track-waveform');
    waveformEl.classList.remove('audio-player__track-waveform--visible');
    const playArrowEl = el.querySelector('.audio-player__track-play-arrow');
    playArrowEl.classList.add('audio-player__track-play-arrow--visible');
}

// Toggle tracks playing style on Plyr playing event
function toggleTracksPlayingStyleOnPlay() {
    plyr.on('playing', function(e) {
        for (let i = 0; i < audioPlayerPlaylistEls.length; i++) {
            if (audioPlayerPlaylistEls[i].classList.contains('audio-player__active-track')) {
                enableTrackPlayingStyle(audioPlayerPlaylistEls[i]);
            } else {
                disableTrackPlayingStyle(audioPlayerPlaylistEls[i]);
            }
        }
    });
}

// Toggle tracks playing style on Plyr pause event
function toggleTracksPlayingStyleOnPause() {
    plyr.on('pause', function(e) {
        for (let i = 0; i < audioPlayerPlaylistEls.length; i++) {
            disableTrackPlayingStyle(audioPlayerPlaylistEls[i]);
        }
    });
}

// Handle audio player track selection
function handleAudioPlayer() {
    if (plyr && audioPlayerHeaderEl && audioPlayerPlaylistEls.length > 0) {
        audioPlayerPlyrEl.removeAttribute('tabindex'); // Remove tabindex attribute from Plyr element to avoid useless focus
        selectFirstTrack();
        toggleDataAutoScrollOnIntersect();
        toggleDataAutoScrollOnResize();
        setActiveTrackOnPlaylistClick();
        setActiveTrackOnPreviousBtnClick();
        setActiveTrackOnNextBtnClick();
        setActiveTrackOnTrackEnd();
        editPlyrBtnAriaLabelOnMediaEvents();
        editPlyrCurrentTimeOnMediaEvents();
        editPlyrDurationOnLoadedData();
        toggleTracksPlayingStyleOnPlay();
        toggleTracksPlayingStyleOnPause();
    }
}

// MOBILE NAVIGATION //

// Toggle mobile navigation
function toggleMobileNav(e) {
    navBtn.classList.toggle('top-nav__burger-button--close');
    navMenuEl.classList.toggle('top-nav__menu--left-position');
    overlayEl.classList.toggle('overlay--visible');
    document.body.classList.toggle('body--hidden-vertical-overflow');
    showHeader();
}


// Enable mobile navigation
function enableMobileNav(e) {
    navBtn.classList.add('top-nav__burger-button--close');
    navMenuEl.classList.add('top-nav__menu--left-position');
    overlayEl.classList.add('overlay--visible');
    document.body.classList.add('body--hidden-vertical-overflow');
    showHeader();
}

// Disable mobile navigation
function disableMobileNav(e) {
    navBtn.classList.remove('top-nav__burger-button--close');
    navMenuEl.classList.remove('top-nav__menu--left-position');
    overlayEl.classList.remove('overlay--visible');
    document.body.classList.remove('body--hidden-vertical-overflow');
    showHeader();
}

// Toggle mobile navigation on button click event
function toggleMobileNavOnClick() {
    if (navBtn && navMenuEl) {
        navBtn.addEventListener('click', toggleMobileNav);
    }
}

// Enable mobile navigation on menu list focusin event
function enableMobileNavBeforeFocus() {
    if (navMenuEl && navMenuListEls) {
        for (let i = 0; i < navMenuListEls.length; i++) {
            navMenuListEls[i].addEventListener('focusin', function(e) {
                if (window.innerWidth < mediaQuery.desktopWidth) {
                    enableMobileNav(e);
                }
            });
        }
    }
}

// Disable mobile navigation on menu list focusout event
function disableMobileNavAfterFocus() {
    if (navMenuEl && navMenuListEls) {
        for (let i = 0; i < navMenuListEls.length; i++) {
            navMenuListEls[i].addEventListener('focusout', disableMobileNav);
        }
    }
}

// Disable mobile navigation menu on touchstart and touchmove events (swipe right)
function disableMobileNavOnSwipeRight() {
    navMenuEl.addEventListener('touchstart', function(e) {
        if (navMenuEl.classList.contains('top-nav__menu--left-position') && touchPosition.initialX === null && touchPosition.initialY === null) {
            touchPosition.initialX = event.touches[0].clientX;
            touchPosition.initialY = event.touches[0].clientY;
        }
    }, {passive: true});
    navMenuEl.addEventListener('touchmove', function(e) {
        if (navMenuEl.classList.contains('top-nav__menu--left-position') && touchPosition.initialX !== null && touchPosition.initialY !== null) {
            touchPosition.currentX = event.touches[0].clientX;
            touchPosition.currentY = event.touches[0].clientY;
            touchPosition.diffX = touchPosition.initialX - touchPosition.currentX;
            touchPosition.diffY = touchPosition.initialY - touchPosition.currentY;
            if (Math.abs(touchPosition.diffX) > Math.abs(touchPosition.diffY)) {
                if (touchPosition.diffX < 0) {
                    disableMobileNav(e);
                }
            }
            touchPosition.initialX = null;
            touchPosition.initialY = null;
        }
    }, {passive: true});
}

// Handle mobile navigation
function handleMobileNav() {
    toggleMobileNavOnClick();
    enableMobileNavBeforeFocus();
    disableMobileNavAfterFocus();
    disableMobileNavOnSwipeRight();
}

// ANIMATIONS //

// Hide header when user scrolls down 400px
function hideHeader() {
    if (headerEl && scrollOffset.header < window.pageYOffset - 400) {
        headerEl.classList.add('header--hidden-position');
        scrollOffset.header = window.pageYOffset;
    }
}

// Show header
function showHeader() {
    if (headerEl) {
        headerEl.classList.remove('header--hidden-position');
        scrollOffset.header = window.pageYOffset;
    }
}

// Move search bar to top position
function moveSearchBarToTopPosition() {
    if (headerEl && searchBarEl) {
        if (headerEl.classList.contains('header--hidden-position') && searchBarEl.getBoundingClientRect().top <= headerEl.getBoundingClientRect().height) {
            searchBarEl.classList.add('library__search-bar--top-position');
        }
    }
}

// Remove search bar from top position
function removeSearchBarFromTopPosition() {
    if (searchBarEl) {
        searchBarEl.classList.remove('library__search-bar--top-position');
    }
}

function toggleSearchBarStickyStyle() {
    if (headerEl && searchBarEl) {
        if (searchBarEl.getBoundingClientRect().top <= headerEl.getBoundingClientRect().height) {
            headerEl.classList.add('header--no-box-shadow');
            searchBarEl.classList.add('library__search-bar--sticky-style');
        } else {
            headerEl.classList.remove('header--no-box-shadow');
            searchBarEl.classList.remove('library__search-bar--sticky-style');
        }
    }
}

// Move audio player to top position
function moveAudioPlayerToTopPosition() {
    if (headerEl && audioPlayerHeaderEl) {
        if (headerEl.classList.contains('header--hidden-position') && audioPlayerHeaderEl.getBoundingClientRect().top <= headerEl.getBoundingClientRect().height) {
            audioPlayerHeaderEl.classList.add('audio-player__header--top-position');
        }
    }
}

// Remove audio player from top position
function removeAudioPlayerFromTopPosition() {
    if (audioPlayerHeaderEl) {
        audioPlayerHeaderEl.classList.remove('audio-player__header--top-position');
    }
}

// Make audio player wider or narrower depending on its position
function toggleAudioPlayerStickyStyle() {
    if (headerEl && audioPlayerHeaderEl) {
        if (audioPlayerHeaderEl.getBoundingClientRect().top <= headerEl.getBoundingClientRect().height) {
            headerEl.classList.add('header--no-box-shadow');
            audioPlayerHeaderEl.classList.add('audio-player__header--sticky-style');
        } else {
            headerEl.classList.remove('header--no-box-shadow');
            audioPlayerHeaderEl.classList.remove('audio-player__header--sticky-style');
        }
    }
}

// Toggle back to top button
function toggleBackToTopBtn() {
    if (backToTopBtn) {
        if (window.pageYOffset >= 800) {
            backToTopBtn.classList.add('back-to-top-button--visible');
        } else {
            backToTopBtn.classList.remove('back-to-top-button--visible');
        }
    }
}

// Animate header, search bar, audio player elements and back to top button on scroll
function animateElementsOnScroll() {
    document.addEventListener('scroll', function() {
        if (scrollOffset.start < window.pageYOffset) { // Scroll down
            hideHeader();
            moveSearchBarToTopPosition();
            moveAudioPlayerToTopPosition();
        } else if (scrollOffset.start > window.pageYOffset) { // Scroll up
            showHeader();
            removeSearchBarFromTopPosition();
            removeAudioPlayerFromTopPosition();
        }
        toggleSearchBarStickyStyle();
        toggleAudioPlayerStickyStyle();
        toggleBackToTopBtn();
        scrollOffset.start = window.pageYOffset;
    }, {passive: true});
}

// Enable back to top button on focusin event
function enableBackToTopBtnBeforeFocus() {
    if (backToTopBtn) {
        backToTopBtn.addEventListener('focusin', function(e) {
            backToTopBtn.classList.add('back-to-top-button--visible');
        });
    }
}

// Disable back to top button on focusout event
function disableBackToTopBtnAfterFocus() {
    if (backToTopBtn) {
        backToTopBtn.addEventListener('focusout', function(e) {
            if (window.pageYOffset < 800) {
                backToTopBtn.classList.remove('back-to-top-button--visible');
            }
        });
    }
}

// Toggle back to top button on focus
function toggleBackToTopButton() {
    enableBackToTopBtnBeforeFocus();
    disableBackToTopBtnAfterFocus();
}

// IntersectionObserver fade in from bottom callback function
function fadeInFromBottom(entries, observer) {
    entries.forEach(function(entry) {
        if (entry.isIntersecting) {
            entry.target.classList.add('fade-in-from-bottom');
            observer.unobserve(entry.target);
        }
    });
}

// Handle featured audiobooks fade in when intersecting with viewport
function handleFeaturedAudiobooksFadeIn() {
    if (mostRecentAudiobook && mostViewedAudiobook) {
        const fadeInFromBottomObserver = new IntersectionObserver(fadeInFromBottom);
        fadeInFromBottomObserver.observe(mostRecentAudiobook);
        fadeInFromBottomObserver.observe(mostViewedAudiobook);
    }
}

// Handle animations on scroll, on focus and when elements intersect with viewport
function handleAnimations() {
    animateElementsOnScroll();
    toggleBackToTopButton();
    handleFeaturedAudiobooksFadeIn();
}

// ----------------------------------------------------------------------------
// PROGRAM
// ----------------------------------------------------------------------------

document.addEventListener('DOMContentLoaded', function() {

    document.addEventListener("touchstart", function() {},false); // Enable CSS :active pseudo-class in Safari Mobile

    // BOOKMARKING SYSTEM //

    toggleBookmarksOnClick();

    // AUDIO PLAYER //

    handleAudioPlayer();

    // MOBILE NAVIGATION //

    handleMobileNav();

    // ANIMATIONS //

    handleAnimations();
});

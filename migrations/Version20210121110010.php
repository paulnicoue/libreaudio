<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121110010 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE audiobook_licence (audiobook_id INT NOT NULL, licence_id INT NOT NULL, INDEX IDX_AE8654CCED9E55A4 (audiobook_id), INDEX IDX_AE8654CC26EF07C9 (licence_id), PRIMARY KEY(audiobook_id, licence_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audiobook_category (audiobook_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_EC2A724AED9E55A4 (audiobook_id), INDEX IDX_EC2A724A12469DE2 (category_id), PRIMARY KEY(audiobook_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audiobook_user (audiobook_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3594F1D7ED9E55A4 (audiobook_id), INDEX IDX_3594F1D7A76ED395 (user_id), PRIMARY KEY(audiobook_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE audiobook_licence ADD CONSTRAINT FK_AE8654CCED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_licence ADD CONSTRAINT FK_AE8654CC26EF07C9 FOREIGN KEY (licence_id) REFERENCES licence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_category ADD CONSTRAINT FK_EC2A724AED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_category ADD CONSTRAINT FK_EC2A724A12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_user ADD CONSTRAINT FK_3594F1D7ED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_user ADD CONSTRAINT FK_3594F1D7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE archive_file ADD audiobook_id INT NOT NULL');
        $this->addSql('ALTER TABLE archive_file ADD CONSTRAINT FK_BCBAE08BED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id)');
        $this->addSql('CREATE INDEX IDX_BCBAE08BED9E55A4 ON archive_file (audiobook_id)');
        $this->addSql('ALTER TABLE audio_file ADD audiobook_id INT NOT NULL');
        $this->addSql('ALTER TABLE audio_file ADD CONSTRAINT FK_C32E2A4CED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id)');
        $this->addSql('CREATE INDEX IDX_C32E2A4CED9E55A4 ON audio_file (audiobook_id)');
        $this->addSql('ALTER TABLE audiobook ADD source_id INT DEFAULT NULL, ADD creation_date DATE NOT NULL, ADD duration TIME NOT NULL, ADD view_count INT NOT NULL, ADD summary LONGTEXT NOT NULL, ADD file_directory VARCHAR(180) NOT NULL');
        $this->addSql('ALTER TABLE audiobook ADD CONSTRAINT FK_E860B468953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('CREATE INDEX IDX_E860B468953C1C61 ON audiobook (source_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE audiobook_licence');
        $this->addSql('DROP TABLE audiobook_category');
        $this->addSql('DROP TABLE audiobook_user');
        $this->addSql('ALTER TABLE archive_file DROP FOREIGN KEY FK_BCBAE08BED9E55A4');
        $this->addSql('DROP INDEX IDX_BCBAE08BED9E55A4 ON archive_file');
        $this->addSql('ALTER TABLE archive_file DROP audiobook_id');
        $this->addSql('ALTER TABLE audio_file DROP FOREIGN KEY FK_C32E2A4CED9E55A4');
        $this->addSql('DROP INDEX IDX_C32E2A4CED9E55A4 ON audio_file');
        $this->addSql('ALTER TABLE audio_file DROP audiobook_id');
        $this->addSql('ALTER TABLE audiobook DROP FOREIGN KEY FK_E860B468953C1C61');
        $this->addSql('DROP INDEX IDX_E860B468953C1C61 ON audiobook');
        $this->addSql('ALTER TABLE audiobook DROP source_id, DROP creation_date, DROP duration, DROP view_count, DROP summary, DROP file_directory');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121094843 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE audiobook_reader (audiobook_id INT NOT NULL, reader_id INT NOT NULL, INDEX IDX_86F723C4ED9E55A4 (audiobook_id), INDEX IDX_86F723C41717D737 (reader_id), PRIMARY KEY(audiobook_id, reader_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE audiobook_reader ADD CONSTRAINT FK_86F723C4ED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_reader ADD CONSTRAINT FK_86F723C41717D737 FOREIGN KEY (reader_id) REFERENCES reader (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook ADD publication_year SMALLINT DEFAULT NULL, ADD first_publication_year SMALLINT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE audiobook_reader');
        $this->addSql('ALTER TABLE audiobook DROP publication_year, DROP first_publication_year');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}

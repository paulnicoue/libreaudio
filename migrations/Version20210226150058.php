<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210226150058 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE audio_file CHANGE duration duration VARCHAR(180) NOT NULL, CHANGE format type VARCHAR(180) NOT NULL');
        $this->addSql('ALTER TABLE audiobook CHANGE duration duration VARCHAR(180) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE audio_file CHANGE duration duration TIME NOT NULL, CHANGE type format VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE audiobook CHANGE duration duration TIME NOT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}

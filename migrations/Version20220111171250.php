<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220111171250 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E860B468989D9B62 ON audiobook (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDAFD8C8989D9B62 ON author (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_E860B468989D9B62 ON audiobook');
        $this->addSql('DROP INDEX UNIQ_BDAFD8C8989D9B62 ON author');
    }
}

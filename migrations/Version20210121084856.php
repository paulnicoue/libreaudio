<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121084856 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE audiobook (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audiobook_author (audiobook_id INT NOT NULL, author_id INT NOT NULL, INDEX IDX_F7677230ED9E55A4 (audiobook_id), INDEX IDX_F7677230F675F31B (author_id), PRIMARY KEY(audiobook_id, author_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audiobook_translator (audiobook_id INT NOT NULL, translator_id INT NOT NULL, INDEX IDX_5ADAF0DDED9E55A4 (audiobook_id), INDEX IDX_5ADAF0DD5370E40B (translator_id), PRIMARY KEY(audiobook_id, translator_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audiobook_publisher (audiobook_id INT NOT NULL, publisher_id INT NOT NULL, INDEX IDX_E668E985ED9E55A4 (audiobook_id), INDEX IDX_E668E98540C86FCE (publisher_id), PRIMARY KEY(audiobook_id, publisher_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE audiobook_author ADD CONSTRAINT FK_F7677230ED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_author ADD CONSTRAINT FK_F7677230F675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_translator ADD CONSTRAINT FK_5ADAF0DDED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_translator ADD CONSTRAINT FK_5ADAF0DD5370E40B FOREIGN KEY (translator_id) REFERENCES translator (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_publisher ADD CONSTRAINT FK_E668E985ED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_publisher ADD CONSTRAINT FK_E668E98540C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE audiobook_author DROP FOREIGN KEY FK_F7677230ED9E55A4');
        $this->addSql('ALTER TABLE audiobook_translator DROP FOREIGN KEY FK_5ADAF0DDED9E55A4');
        $this->addSql('ALTER TABLE audiobook_publisher DROP FOREIGN KEY FK_E668E985ED9E55A4');
        $this->addSql('DROP TABLE audiobook');
        $this->addSql('DROP TABLE audiobook_author');
        $this->addSql('DROP TABLE audiobook_translator');
        $this->addSql('DROP TABLE audiobook_publisher');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420113246 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE audiobook_publisher DROP FOREIGN KEY FK_E668E98540C86FCE');
        $this->addSql('DROP TABLE audiobook_publisher');
        $this->addSql('DROP TABLE publisher');
        $this->addSql('ALTER TABLE audiobook DROP publication_year');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE audiobook_publisher (audiobook_id INT NOT NULL, publisher_id INT NOT NULL, INDEX IDX_E668E98540C86FCE (publisher_id), INDEX IDX_E668E985ED9E55A4 (audiobook_id), PRIMARY KEY(audiobook_id, publisher_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE publisher (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE audiobook_publisher ADD CONSTRAINT FK_E668E98540C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook_publisher ADD CONSTRAINT FK_E668E985ED9E55A4 FOREIGN KEY (audiobook_id) REFERENCES audiobook (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE audiobook ADD publication_year SMALLINT DEFAULT NULL');
    }
}

# What is LibreAudio?

![LibreAudio website screenshot](assets/images/libreaudio-readme-03-1500px.png)

[LibreAudio](https://libreaudio.fr/) is a **free, open-source audio book library**. It was designed with the aim of being **accessible to the greatest possible number of people**. All the recordings on this website are released under free software licenses and originate from the work of volunteers who lent their voices. These people help in building renowned audio book platforms, such as [LibriVox](https://librivox.org/), [AudioCité](https://www.audiocite.net/) or [Littérature Audio](http://www.litteratureaudio.com/). With respect to these extensive libraries, LibreAudio simply aspires to offer a different interface intended to match our modern web and digital tools uses.

It should also be noted that LibreAudio is a new project that will continue growing and expanding. Some of the future development focuses encompass the updating of the database (a key part of this library) as well as the improvement of the website usability for diverse audiences.

![LibreAudio website screenshot](assets/images/libreaudio-readme-04-1500px.png)

![LibreAudio website screenshot](assets/images/libreaudio-readme-05-1500px.png)

Copyright (C) 2021 [Paul Nicoué](https://paulnicoue.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

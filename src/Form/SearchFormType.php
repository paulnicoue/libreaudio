<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Utils\DataSearcher;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class, [ // Query string
                'required' => false,
                'label' => false,
                'error_bubbling' => true,
                'attr' => [
                    'placeholder' => 'Audiobook or author',
                    'title' => 'Enter audiobook title or author name',
                    'class' => 'search__input',
                ],
            ])
            ->add('category', EntityType::class, [ // Categories select tag
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $categoryRepository) {
                    return $categoryRepository->createQueryBuilder('category')
                        ->orderBy('category.name', 'ASC');
                },
                'required' => false,
                'label' => false,
                'choice_label' => function($category) {
                    return $category->getName();
                },
                'placeholder' => 'All categories',
                'expanded' => false,
                'multiple' => false,
                'error_bubbling' => true,
                'attr' => [
                    'title' => 'Choose audiobook category',
                    'class' => 'search__select',
                ],
            ])
            ->add('order', ChoiceType::class, [ // Order by most viewed, most recent or alphabetically select tag
                'required' => true,
                'label' => false,
                'choices' => [
                    'Most recent' => 'mr',
                    'Most viewed' => 'mv',
                    'From A to Z' => 'az',
                ],
                'expanded' => false,
                'multiple' => false,
                'error_bubbling' => true,
                'attr' => [
                    'title' => 'Choose audiobook display order',
                    'class' => 'search__select',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => DataSearcher::class,
           'method' => 'GET',
           'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}

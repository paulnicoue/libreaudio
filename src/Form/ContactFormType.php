<?php

declare(strict_types=1);

namespace App\Form;

use App\Utils\ContactHandler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;


class ContactFormType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Name',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your name.',
                    ]),
                ],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter your email.',
                    ]),
                ],
            ])
            ->add('subject', TextType::class, [
                'required' => true,
                'label' => 'Subject',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a subject.',
                    ]),
                ],
            ])
            ->add('message', TextareaType::class, [
                'required' => true,
                'label' => 'Message',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a message.',
                    ]),
                ],
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => ContactHandler::class
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\SourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SourceRepository::class)
 */
class Source
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $fileDirectory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $websiteLink;

    /**
     * @ORM\OneToMany(targetEntity=Audiobook::class, mappedBy="source")
     */
    private $audiobooks;

    public function __construct()
    {
        $this->audiobooks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFileDirectory(): ?string
    {
        return $this->fileDirectory;
    }

    public function setFileDirectory(string $fileDirectory): self
    {
        $this->fileDirectory = $fileDirectory;

        return $this;
    }

    public function getWebsiteLink(): ?string
    {
        return $this->websiteLink;
    }

    public function setWebsiteLink(?string $websiteLink): self
    {
        $this->websiteLink = $websiteLink;

        return $this;
    }

    /**
     * @return Collection|Audiobook[]
     */
    public function getAudiobooks(): Collection
    {
        return $this->audiobooks;
    }

    public function addAudiobook(Audiobook $audiobook): self
    {
        if (!$this->audiobooks->contains($audiobook)) {
            $this->audiobooks[] = $audiobook;
            $audiobook->setSource($this);
        }

        return $this;
    }

    public function removeAudiobook(Audiobook $audiobook): self
    {
        if ($this->audiobooks->removeElement($audiobook)) {
            // set the owning side to null (unless already changed)
            if ($audiobook->getSource() === $this) {
                $audiobook->setSource(null);
            }
        }

        return $this;
    }
    
    public function __toString(): string
    {
        return $this->name;
    }
}

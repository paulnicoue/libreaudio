<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AudiobookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\AudiobookRepository::class)
 */
class Audiobook
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="audiobooks")
     */
    private $authors;

    /**
     * @ORM\ManyToMany(targetEntity=Translator::class, inversedBy="audiobooks")
     */
    private $translators;

    /**
     * @ORM\ManyToMany(targetEntity=Reader::class, inversedBy="audiobooks")
     */
    private $readers;

    /**
     * @ORM\ManyToOne(targetEntity=Source::class, inversedBy="audiobooks")
     */
    private $source;

    /**
     * @ORM\ManyToMany(targetEntity=Licence::class, inversedBy="audiobooks")
     */
    private $licences;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="audiobooks")
     */
    private $categories;

    /**
     * @ORM\Column(type="date")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     */
    private $viewCount;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="audiobooks")
     */
    private $users;

    /**
     * @ORM\Column(type="text")
     */
    private $summary;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $fileDirectory;

    /**
     * @ORM\OneToMany(targetEntity=AudioFile::class, mappedBy="audiobook")
     */
    private $audioFiles;

    /**
     * @ORM\OneToMany(targetEntity=ArchiveFile::class, mappedBy="audiobook")
     */
    private $archiveFiles;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDisplayed;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->translators = new ArrayCollection();
        $this->publishers = new ArrayCollection();
        $this->readers = new ArrayCollection();
        $this->licences = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->audioFiles = new ArrayCollection();
        $this->archiveFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->authors->removeElement($author);

        return $this;
    }

    /**
     * @return Collection|Translator[]
     */
    public function getTranslators(): Collection
    {
        return $this->translators;
    }

    public function addTranslator(Translator $translator): self
    {
        if (!$this->translators->contains($translator)) {
            $this->translators[] = $translator;
        }

        return $this;
    }

    public function removeTranslator(Translator $translator): self
    {
        $this->translators->removeElement($translator);

        return $this;
    }

    /**
     * @return Collection|Reader[]
     */
    public function getReaders(): Collection
    {
        return $this->readers;
    }

    public function addReader(Reader $reader): self
    {
        if (!$this->readers->contains($reader)) {
            $this->readers[] = $reader;
        }

        return $this;
    }

    public function removeReader(Reader $reader): self
    {
        $this->readers->removeElement($reader);

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|Licence[]
     */
    public function getLicences(): Collection
    {
        return $this->licences;
    }

    public function addLicence(Licence $licence): self
    {
        if (!$this->licences->contains($licence)) {
            $this->licences[] = $licence;
        }

        return $this;
    }

    public function removeLicence(Licence $licence): self
    {
        $this->licences->removeElement($licence);

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getViewCount(): ?int
    {
        return $this->viewCount;
    }

    public function setViewCount(int $viewCount): self
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getFileDirectory(): ?string
    {
        return $this->fileDirectory;
    }

    public function setFileDirectory(string $fileDirectory): self
    {
        $this->fileDirectory = $fileDirectory;

        return $this;
    }

    /**
     * @return Collection|AudioFile[]
     */
    public function getAudioFiles(): Collection
    {
        return $this->audioFiles;
    }

    public function addAudioFile(AudioFile $audioFile): self
    {
        if (!$this->audioFiles->contains($audioFile)) {
            $this->audioFiles[] = $audioFile;
            $audioFile->setAudiobook($this);
        }

        return $this;
    }

    public function removeAudioFile(AudioFile $audioFile): self
    {
        if ($this->audioFiles->removeElement($audioFile)) {
            // set the owning side to null (unless already changed)
            if ($audioFile->getAudiobook() === $this) {
                $audioFile->setAudiobook(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArchiveFile[]
     */
    public function getArchiveFiles(): Collection
    {
        return $this->archiveFiles;
    }

    public function addArchiveFile(ArchiveFile $archiveFile): self
    {
        if (!$this->archiveFiles->contains($archiveFile)) {
            $this->archiveFiles[] = $archiveFile;
            $archiveFile->setAudiobook($this);
        }

        return $this;
    }

    public function removeArchiveFile(ArchiveFile $archiveFile): self
    {
        if ($this->archiveFiles->removeElement($archiveFile)) {
            // set the owning side to null (unless already changed)
            if ($archiveFile->getAudiobook() === $this) {
                $archiveFile->setAudiobook(null);
            }
        }

        return $this;
    }

    public function getIsDisplayed(): ?bool
    {
        return $this->isDisplayed;
    }

    public function setIsDisplayed(bool $isDisplayed): self
    {
        $this->isDisplayed = $isDisplayed;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}

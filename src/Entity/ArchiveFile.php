<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ArchiveFileRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArchiveFileRepository::class)
 */
class ArchiveFile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $format;

    /**
     * @ORM\Column(type="float")
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity=Audiobook::class, inversedBy="archiveFiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $audiobook;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getAudiobook(): ?Audiobook
    {
        return $this->audiobook;
    }

    public function setAudiobook(?Audiobook $audiobook): self
    {
        $this->audiobook = $audiobook;

        return $this;
    }
    
    public function __toString()
    {
        return $this->name;
    }
}

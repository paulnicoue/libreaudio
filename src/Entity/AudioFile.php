<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AudioFileRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AudioFileRepository::class)
 */
class AudioFile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Audiobook::class, inversedBy="audioFiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $audiobook;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $duration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $format): self
    {
        $this->type = $format;

        return $this;
    }

    public function getAudiobook(): ?Audiobook
    {
        return $this->audiobook;
    }

    public function setAudiobook(?Audiobook $audiobook): self
    {
        $this->audiobook = $audiobook;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }
    
    public function __toString()
    {
        return $this->name;
    }
}

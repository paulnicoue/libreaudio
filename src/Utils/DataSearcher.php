<?php

declare(strict_types=1);

namespace App\Utils;

use App\Entity\Category;

class DataSearcher
{
    private ?string $query;
    
    private ?Category $category;
    
    private ?string $order;
    
    public function __construct()
    {
        $this->query = null;
        $this->category = null;
        $this->order = null;
    }
    
    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(?string $query): self
    {
        $this->query = $query;

        return $this;
    }
    
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
    
    public function getOrder(): ?string
    {
        return $this->order;
    }

    public function setOrder(?string $order): self
    {
        $this->order = $order;

        return $this;
    }
}

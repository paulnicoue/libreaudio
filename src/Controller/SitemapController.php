<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\AudiobookRepository;
use App\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapController extends AbstractController
{
    /**
     * Sitemap
     *
     * @Route("/sitemap.xml", name="app_sitemap", defaults={"_format"="xml"})
     */
    public function sitemap(AudiobookRepository $audiobookRepository, AuthorRepository $authorRepository, Request $request): Response
    {
        // Initialize empty URLs array
        $urls = [];

        // Add static URLs
        $urls[] = [
            'loc' => $this->generateUrl('app_index', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'changefreq' => 'weekly',
            'priority' => '1.0'
        ];
        $urls[] = [
            'loc' => $this->generateUrl('app_library', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'changefreq' => 'weekly',
            'priority' => '0.8'
        ];
        $urls[] = ['loc' => $this->generateUrl('app_about', [], UrlGeneratorInterface::ABSOLUTE_URL)];
        $urls[] = ['loc' => $this->generateUrl('app_login', [], UrlGeneratorInterface::ABSOLUTE_URL)];
        $urls[] = ['loc' => $this->generateUrl('app_contact', [], UrlGeneratorInterface::ABSOLUTE_URL)];
        $urls[] = ['loc' => $this->generateUrl('app_conditions', [], UrlGeneratorInterface::ABSOLUTE_URL)];

        // Get displayed audiobooks and authors from repositories
        $audiobooks = $audiobookRepository->findBy(['isDisplayed' => true]);
        $authors = $authorRepository->findAll();

        // Add audiobooks dynamic URLs
        foreach ($audiobooks as $audiobook) {
            $urls[] = ['loc' => $this->generateUrl('app_audiobook', [
                'slug' => $audiobook->getSlug()
            ], UrlGeneratorInterface::ABSOLUTE_URL)];
        }

        // Add authors dynamic URLs
        foreach ($authors as $author) {
            $urls[] = ['loc' => $this->generateUrl('app_author', [
                'slug' => $author->getSlug()
            ], UrlGeneratorInterface::ABSOLUTE_URL)];
        }

        // Create new Response object with sitemap template
        $response = new Response(
            $this->renderView('sitemap/sitemap.html.twig', [
                'urls' => $urls
            ])
        );

        // Set XML Response headers
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}

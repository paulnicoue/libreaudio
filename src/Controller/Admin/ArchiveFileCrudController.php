<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\ArchiveFile;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArchiveFileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArchiveFile::class;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('format'),
            NumberField::new('size'),
            AssociationField::new('audiobook')
        ];
    }
}

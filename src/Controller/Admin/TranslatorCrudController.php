<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Translator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TranslatorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Translator::class;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('audiobooks')
        ];
    }
}

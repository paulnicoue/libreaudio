<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\AudioFile;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AudioFileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AudioFile::class;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('title'),
            TextField::new('type'),
            TextField::new('duration'),
            AssociationField::new('audiobook')
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\ArchiveFile;
use App\Entity\Audiobook;
use App\Entity\AudioFile;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Licence;
use App\Entity\Publisher;
use App\Entity\Reader;
use App\Entity\Source;
use App\Entity\Translator;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/administration", name="app_admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(AudiobookCrudController::class)->generateUrl()); // Redirect to AudiobookCrudController
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('LibreAudio')
            ->setFaviconPath('images/libreaudio-favicon-32px.png')
            ->renderContentMaximized();
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('build/admin.css');
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setDateFormat('dd:MM:yyyy')
            ->setTimeFormat('HH:mm:ss')
            ->setDateTimeFormat('dd:MM:yyyy HH:mm:ss');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::section('Links'),
            MenuItem::linkToRoute('Home', 'fas fa-globe', 'app_index'),
            MenuItem::linkToLogout('Logout', 'fas fa-sign-out-alt'),
            MenuItem::section('Entities'),
            MenuItem::linkToCrud('Audiobooks', 'fas fa-book-open', Audiobook::class),
            MenuItem::linkToCrud('Authors', 'fas fa-feather-alt', Author::class),
            MenuItem::linkToCrud('Translators', 'fas fa-language', Translator::class),
            MenuItem::linkToCrud('Readers', 'fas fa-microphone', Reader::class),
            MenuItem::linkToCrud('Sources', 'fas fa-link', Source::class),
            MenuItem::linkToCrud('Licences', 'fab fa-creative-commons', Licence::class),
            MenuItem::linkToCrud('Categories', 'fas fa-tag', Category::class),
            MenuItem::linkToCrud('Archive files', 'fas fa-file-archive', ArchiveFile::class),
            MenuItem::linkToCrud('Audio files', 'fas fa-file-audio', AudioFile::class),
            MenuItem::linkToCrud('Users', 'fas fa-users', User::class)
        ];
    }
}

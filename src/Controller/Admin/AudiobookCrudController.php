<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Audiobook;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AudiobookCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Audiobook::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextField::new('slug'),
            BooleanField::new('isDisplayed'),
            AssociationField::new('authors'),
            AssociationField::new('translators'),
            AssociationField::new('readers'),
            AssociationField::new('source'),
            AssociationField::new('licences'),
            AssociationField::new('categories'),
            DateField::new('creationDate'),
            TextField::new('duration'),
            IntegerField::new('viewCount'),
            AssociationField::new('users'),
            TextareaField::new('summary'),
            TextField::new('fileDirectory'),
            AssociationField::new('archiveFiles'),
            AssociationField::new('audioFiles')
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ContactFormType;
use App\Utils\ContactHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    /**
     * Display contact page and send email
     * 
     * @Route("/contact", name="app_contact")
     */
    public function contact(Request $request, MailerInterface $mailer, TranslatorInterface $translator): Response
    {
        $contactHandler = new ContactHandler();
        $antiSpamField = [
            // Time protection
            'antispam_time' => true,
            'antispam_time_min' => 5,
            'antispam_time_max' => 3600,
            // Honeypot protection
            'antispam_honeypot' => true,
            'antispam_honeypot_class' => null,
            'antispam_honeypot_field' => 'email_address'
        ];
        $form = $this->createForm(ContactFormType::class, $contactHandler, $antiSpamField);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $email = (new Email())
                ->from(new Address('contact@libreaudio.fr', $contactHandler->getName()))
                ->to('contact@libreaudio.fr')
                ->replyTo($contactHandler->getEmail())
                ->subject($contactHandler->getSubject())
                ->text($contactHandler->getMessage());
            
            $mailer->send($email);
            
            $this->addFlash('contact_success', $translator->trans('Your message has been sent.'));
            
            return $this->redirectToRoute('app_contact');
        }
        
        return $this->render('contact.html.twig', [
            'contactForm' => $form->createView()
        ]);
    }
}

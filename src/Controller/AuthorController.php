<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Author;
use App\Form\SearchFormType;
use App\Repository\AudiobookRepository;
use App\Utils\DataSearcher;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    /**
     * Author page
     *
     * @Route("/auteurs-et-autrices/{slug}", name="app_author")
     */
    public function author(AudiobookRepository $audiobookRepository, Author $author, Request $request, PaginatorInterface $paginator): Response
    {
        $author = $this->getDoctrine()->getRepository(Author::class)->find($author);

        $dataSearcher = new DataSearcher();
        $form = $this->createForm(SearchFormType::class, $dataSearcher);
        $form->handleRequest($request);

        $queryBuilder = $audiobookRepository->findAudiobooksByAuthor($author, $dataSearcher);

        $audiobooks = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1), // Requested page number on initial page load
            8 // Limit of objects (audiobooks) per page
        );

        return $this->render('author.html.twig', [
            'searchForm' => $form->createView(),
            'author' => $author,
            'audiobooks' => $audiobooks
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConditionsController extends AbstractController
{
    /**
     * Terms and conditions page
     * 
     * @Route("/cgu", name="app_conditions")
     */
    public function conditions(): Response
    {
        return $this->render('conditions.html.twig', []);
    }
}

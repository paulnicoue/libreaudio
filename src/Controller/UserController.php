<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\SearchFormType;
use App\Repository\AudiobookRepository;
use App\Utils\DataSearcher;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * User page
     *
     * @Route("/mon-compte/favoris", name="app_user")
     */
    public function user(AudiobookRepository $audiobookRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $user = $this->getUser(); // Get current user

        $dataSearcher = new DataSearcher();
        $form = $this->createForm(SearchFormType::class, $dataSearcher);
        $form->handleRequest($request);

        $queryBuilder = $audiobookRepository->findAudiobooksByUser($user, $dataSearcher);

        $audiobooks = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1), // Requested page number on initial page load
            8 // Limit of objects (audiobooks) per page
        );

        return $this->render('user.html.twig', [
            'searchForm' => $form->createView(),
            'user' => $user,
            'audiobooks' => $audiobooks
        ]);
    }
}

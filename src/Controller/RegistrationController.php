<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use App\Security\LoginFormAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;
    private $security;

    public function __construct(EmailVerifier $emailVerifier, Security $security)
    {
        $this->emailVerifier = $emailVerifier;
        $this->security = $security;
    }

    /**
     * Display registration page and process form
     *
     * @Route("/inscription", name="app_registration")
     */
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, TranslatorInterface $translator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Hash the plain password
            $user->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // Generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_registration_verification_process', $user,
                (new TemplatedEmail())
                    ->from(new Address('contact@libreaudio.fr', 'LibreAudio'))
                    ->to($user->getEmail())
                    ->subject($translator->trans('Registration validation'))
                    ->htmlTemplate('registration/registration_email.html.twig')
            );

            $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // Firewall name in security.yaml
            );

            // Redirect to the email verification page when the registration form is submitted
            return $this->redirectToRoute('app_registration_verification');
        }

        return $this->render('registration/registration.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * Display email verification page
     *
     * @Route("/inscription/verification", name="app_registration_verification")
     */
    public function displayEmailVerification(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->render('registration/verification.html.twig', []);
    }

    /**
     * Send email verification
     *
     * @Route("/inscription/verification/email", name="app_registration_verification_email")
     */
    public function sendEmailVerification(Request $request, TranslatorInterface $translator): Response
    {
        // Get the logged in user from the Security service
        $user = $this->security->getUser();

        // Generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation('app_registration_verification_process', $user,
            (new TemplatedEmail())
                ->from(new Address('contact@libreaudio.fr', 'LibreAudio'))
                ->to($user->getEmail())
                ->subject($translator->trans('Registration validation'))
                ->htmlTemplate('registration/registration_email.html.twig')
        );

        // Redirect to the verification page after sending the verification email
        return $this->redirectToRoute('app_registration_verification');
    }

    /**
     * Process email verification
     *
     * @Route("/inscription/verification/processus", name="app_registration_verification_process")
     */
    public function processEmailVerification(Request $request, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // Validate the email confirmation link, sets User::isVerified=true and persist
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());
            // Redirect to the verification page in case of failure
            return $this->redirectToRoute('app_registration_verification');
        }

        $this->addFlash('verify_email_success', $translator->trans('Your email has been verified.'));

        // Redirect to the verification page in case of success
        return $this->redirectToRoute('app_registration_verification');
    }
}

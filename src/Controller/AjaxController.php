<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Audiobook;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * Home page
     *
     * @Route("/ajax", name="app_ajax")
     */
    public function ajax(): Response
    {
        $request = Request::createFromGlobals();
        $audiobookId = $request->query->get('audiobookId'); // Get AJAX request
        $audiobook = $this->getDoctrine()->getRepository(Audiobook::class)->find($audiobookId); // Find corresponding audiobook by id

        $user = $this->getUser(); // Get current user
        $userAudiobooks = $user->getAudiobooks(); // Get user's audiobooks

        if ($userAudiobooks->contains($audiobook)) { // Remove or add audiobook in user's audiobooks
            $user->removeAudiobook($audiobook);
        } else {
            $user->addAudiobook($audiobook);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush(); // Update database

        $audiobookTitle = $audiobook->getTitle(); // Get audiobook title
        return new JsonResponse($audiobookTitle); // Return audiobook title
    }
}

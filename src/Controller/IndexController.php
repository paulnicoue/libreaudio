<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\SearchFormType;
use App\Repository\AudiobookRepository;
use App\Repository\ReaderRepository;
use App\Utils\DataSearcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * Home page
     *
     * @Route("/", name="app_index")
     */
    public function index(AudiobookRepository $audiobookRepository, ReaderRepository $readerRepository, Request $request): Response
    {

        $audiobookCount = $audiobookRepository->countAudiobooks();
        $readerCount = $readerRepository->countReaders();

        $dataSearcher = new DataSearcher();
        $form = $this->createForm(SearchFormType::class, $dataSearcher);
        $form->handleRequest($request);

        $mostRecentAudiobook = $audiobookRepository->findOneBy(
            ['isDisplayed' => true],
            ['creationDate' => 'DESC', 'title' => 'ASC']
        );

        $mostViewedAudiobook = $audiobookRepository->findOneBy(
            ['isDisplayed' => true],
            ['viewCount' => 'DESC', 'title' => 'ASC']
        );

        if ($form->isSubmitted() && $form->isValid()) {
            // Redirect to app_library route
            return $this->redirectToRoute('app_library',
                // Maintain the original query string parameters
                $request->query->all()
            );
        } else {
            // Render index template
            return $this->render('index.html.twig', [
                'audiobookCount' => $audiobookCount,
                'readerCount' => $readerCount,
                'searchForm' => $form->createView(),
                'mostRecentAudiobook' => $mostRecentAudiobook,
                'mostViewedAudiobook' => $mostViewedAudiobook
            ]);
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordRequestFormType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    private $resetPasswordHelper;

    public function __construct(ResetPasswordHelperInterface $resetPasswordHelper)
    {
        $this->resetPasswordHelper = $resetPasswordHelper;
    }

    /**
     * Display password reset request page and process form
     *
     * @Route("/reinitialisation/demande", name="app_reset_request")
     */
    public function request(Request $request, MailerInterface $mailer, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSendingPasswordResetEmail(
                $form->get('email')->getData(),
                $mailer,
                $translator,
            );
        }

        return $this->render('reset_password/request.html.twig', [
            'requestForm' => $form->createView(),
        ]);
    }

    /**
     * Display email verification page
     *
     * @Route("/reinitialisation/verification", name="app_reset_verification")
     */
    public function displayEmailVerification(): Response
    {
        // Prevent users from directly accessing this page
        if (!$this->canCheckEmail()) {
            return $this->redirectToRoute('app_reset_request');
        }

        return $this->render('reset_password/verification.html.twig', [
            'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
        ]);
    }

    /**
     * Validate and process the reset URL that the user clicked in the email
     *
     * @Route("/reinitialisation/{token}", name="app_reset")
     */
    public function reset(Request $request, UserPasswordHasherInterface $passwordHasher, string $token = null): Response
    {
        if ($token) {
            // Store the token in session and remove it from the URL to avoid the URL being loaded in a browser and potentially leaking the token to 3rd party JavaScript
            $this->storeTokenInSession($token);

            return $this->redirectToRoute('app_reset');
        }

        $token = $this->getTokenFromSession();
        if (null === $token) {
            throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
        }

        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $exception) {
            $this->addFlash('reset_password_error', sprintf('There was a problem validating your reset request - %s', $exception->getReason()));
            // Redirect to the reset password request page in case of failure
            return $this->redirectToRoute('app_reset_request');
        }

        // The token is valid; allow the user to change their password
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Remove the password reset token (because it should be used only once)
            $this->resetPasswordHelper->removeResetRequest($token);

            // Hash the plain password and set it
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $form->get('plainPassword')->getData()
            );

            $user->setPassword($hashedPassword);
            $this->getDoctrine()->getManager()->flush();

            // Clean up the session after the password has been changed
            $this->cleanSessionAfterReset();

            // Redirect to the home page
            return $this->redirectToRoute('app_login');
        }

        return $this->render('reset_password/reset.html.twig', [
            'resetForm' => $form->createView(),
        ]);
    }

    /**
     * Send password reset email (used when password reset form is submitted)
     */
    private function processSendingPasswordResetEmail(string $emailFormData, MailerInterface $mailer, TranslatorInterface $translator): RedirectResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);

        // Marks that the user is allowed to see the app_reset_verification page.
        $this->setCanCheckEmailInSession();

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return $this->redirectToRoute('app_reset_verification');
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            // Uncomment the following section and change the redirection to 'app_reset_request' to tell the user why a reset email was not sent (caution: this may reveal if a user is registered or not)
            // $this->addFlash('reset_password_error', sprintf(
            //    'There was a problem handling your password reset request - %s',
            //    $e->getReason()
            // ));

            return $this->redirectToRoute('app_reset_verification');
        }

        $email = (new TemplatedEmail())
            ->from(new Address('contact@libreaudio.fr', 'LibreAudio'))
            ->to($user->getEmail())
            ->subject($translator->trans('Password reset'))
            ->htmlTemplate('reset_password/reset_email.html.twig')
            ->context([
                'resetToken' => $resetToken,
            ])
        ;

        $mailer->send($email);

        return $this->redirectToRoute('app_reset_verification');
    }
}

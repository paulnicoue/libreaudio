<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Audiobook;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AudiobookController extends AbstractController
{
    /**
     * Audiobook page
     *
     * @Route("/livres-audio/{slug}", name="app_audiobook")
     */
    public function audiobook(Audiobook $audiobook, Request $request): Response
    {
        $audiobook = $this->getDoctrine()->getRepository(Audiobook::class)->find($audiobook);

        $viewCount = $audiobook->getViewCount();
        $viewCount += 1; // Increment $viewCount
        $audiobook->setViewCount($viewCount);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush(); // Update audiobook in database

        return $this->render('audiobook.html.twig', [

            'audiobook' => $audiobook
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Audiobook;
use App\Entity\Author;
use App\Entity\User;
use App\Utils\DataSearcher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Audiobook|null find($id, $lockMode = null, $lockVersion = null)
 * @method Audiobook|null findOneBy(array $criteria, array $orderBy = null)
 * @method Audiobook[]    findAll()
 * @method Audiobook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AudiobookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Audiobook::class);
    }

    /**
     * Return an array of Audiobook objects depending on search filter options
     *
     * @return Audiobook[]
     */
    public function findAudiobooks(DataSearcher $dataSearcher): QueryBuilder
    {
        $queryBuilder = $this
            ->createQueryBuilder('audiobook')
            ->andWhere('audiobook.isDisplayed = true')
            ->join('audiobook.authors', 'author') // Join audiobook and author tables
            ->join('audiobook.categories', 'category'); // Join audiobook and category tables

        if (!empty($dataSearcher->getQuery())) {
            // Search for audiobooks corresponding to the search field input
            $queryBuilder = $queryBuilder
                ->andWhere('audiobook.title LIKE :query OR author.name LIKE :query')
                ->setParameter('query', "%{$dataSearcher->getQuery()}%");
        }

        if (!empty($dataSearcher->getCategory())) {
            // Search for audiobooks corresponding to the chosen category
            $queryBuilder = $queryBuilder
                ->andWhere('category.id = :category')
                ->setParameter('category', $dataSearcher->getCategory());
        }

        if ($dataSearcher->getOrder() === 'az') {
            // Order audiobooks alphabetically
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.title', 'ASC');
        } elseif ($dataSearcher->getOrder() === 'mv') {
            // Order audiobooks by most viewed (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.viewCount', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        } else {
            // Order audiobooks by most recent (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.creationDate', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        }

        return $queryBuilder;
    }

    /**
     * Return an array of Audiobook objects depending on chosen author and search filter options
     *
     * @return Audiobook[]
     */
    public function findAudiobooksByAuthor(Author $author, DataSearcher $dataSearcher): QueryBuilder
    {
        $queryBuilder = $this
            ->createQueryBuilder('audiobook')
            ->andWhere('audiobook.isDisplayed = true')
            ->join('audiobook.authors', 'author') // Join audiobook and author tables
            ->join('audiobook.categories', 'category'); // Join audiobook and category tables

        $queryBuilder = $queryBuilder
            // Search for audiobooks corresponding to the chosen author
            ->andWhere('author.id = :authorId')
            ->setParameter('authorId', $author->getId());

        if (!empty($dataSearcher->getQuery())) {
            // Search for audiobooks corresponding to the search field input
            $queryBuilder = $queryBuilder
                ->andWhere('audiobook.title LIKE :query')
                ->setParameter('query', "%{$dataSearcher->getQuery()}%");
        }

        if (!empty($dataSearcher->getCategory())) {
            // Search for audiobooks corresponding to the chosen category
            $queryBuilder = $queryBuilder
                ->andWhere('category.id = :category')
                ->setParameter('category', $dataSearcher->getCategory());
        }

        if ($dataSearcher->getOrder() === 'az') {
            // Order audiobooks alphabetically
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.title', 'ASC');
        } elseif ($dataSearcher->getOrder() === 'mv') {
            // Order audiobooks by most viewed (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.viewCount', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        } else {
            // Order audiobooks by most recent (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.creationDate', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        }

        return $queryBuilder;
    }

    /**
     * Return an array of Audiobook objects depending on authenticated user and search filter options
     *
     * @return Audiobook[]
     */
    public function findAudiobooksByUser(User $user, DataSearcher $dataSearcher): QueryBuilder
    {
        $queryBuilder = $this
            ->createQueryBuilder('audiobook')
            ->andWhere('audiobook.isDisplayed = true')
            ->join('audiobook.authors', 'author') // Join audiobook and author tables
            ->join('audiobook.categories', 'category') // Join audiobook and category tables
            ->join('audiobook.users', 'user'); // Join audiobook and user tables

        $queryBuilder = $queryBuilder
            // Search for audiobooks corresponding to authenticated user
            ->andWhere('user.id = :userId')
            ->setParameter('userId', $user->getId());

        if (!empty($dataSearcher->getQuery())) {
            // Search for audiobooks corresponding to the search field input
            $queryBuilder = $queryBuilder
                ->andWhere('audiobook.title LIKE :query OR author.name LIKE :query')
                ->setParameter('query', "%{$dataSearcher->getQuery()}%");
        }

        if (!empty($dataSearcher->getCategory())) {
            // Search for audiobooks corresponding to the chosen category
            $queryBuilder = $queryBuilder
                ->andWhere('category.id = :category')
                ->setParameter('category', $dataSearcher->getCategory());
        }

        if ($dataSearcher->getOrder() === 'az') {
            // Order audiobooks alphabetically
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.title', 'ASC');
        } elseif ($dataSearcher->getOrder() === 'mv') {
            // Order audiobooks by most viewed (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.viewCount', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        } else {
            // Order audiobooks by most recent (and alphabetically if they have the same view count)
            $queryBuilder = $queryBuilder
                ->addOrderBy('audiobook.creationDate', 'DESC')
                ->addOrderBy('audiobook.title', 'ASC');
        }

        return $queryBuilder;
    }

    /**
     * Return the number of Audiobook objects as a string
     *
     * @return string
     */
    public function countAudiobooks(): string
    {
        $count = $this
            ->createQueryBuilder('audiobook')
            ->andWhere('audiobook.isDisplayed = true')
            ->select('count(audiobook.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return $count;
    }
}

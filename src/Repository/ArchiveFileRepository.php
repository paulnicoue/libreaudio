<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ArchiveFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArchiveFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArchiveFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArchiveFile[]    findAll()
 * @method ArchiveFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArchiveFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArchiveFile::class);
    }
}

<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AudioFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AudioFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method AudioFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method AudioFile[]    findAll()
 * @method AudioFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AudioFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AudioFile::class);
    }
}

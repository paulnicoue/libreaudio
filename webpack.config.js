const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore

    // ----------------------------------------------------------------------------
    // PATHS
    // ----------------------------------------------------------------------------

    .setOutputPath('public/build/')
    .setPublicPath('/build')

    // ----------------------------------------------------------------------------
    // ENTRIES
    // ----------------------------------------------------------------------------

    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('admin', './assets/css/admin.css')
    .addStyleEntry('main', './assets/css/main.css')
    .copyFiles({
        from: './assets/images',
        to: 'images/[path][name].[hash:8].[ext]'
    })

    // ----------------------------------------------------------------------------
    // FEATURES
    // ----------------------------------------------------------------------------

    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
    .enablePostCssLoader()
;

module.exports = Encore.getWebpackConfig();
